#!/bin/bash

grep -P "\d+\.\d+\.\d+\.\d+" error_log | grep "\[error\]" error_log | awk '{print ( $1" "$2" "$3" " $5)}' | uniq --count
